'use strict';

const timers = {};

module.exports = {
  now: () => new Date(),
  start: function start(name) {
    let time = null;
    if (name) {
      time = new Date();
      timers[name] = time;
    }
    return time;
  },
  end: function end(name) {
    let result = null;
    if (timers[name]) {
      result = new Date().getTime() - timers[name].getTime();
      delete timers[name];
    }
    return result;
  }
};
