'use strict';

import hot from 'webpack-hot-middleware/client';
import launchEditorEndpoint from 'react-dev-utils/launchEditorEndpoint';
import formatWebpackMessages from 'react-dev-utils/formatWebpackMessages';
import * as ErrorOverlay from 'react-error-overlay';

ErrorOverlay.setEditorHandler(errorLocation => {
  fetch(
    `${launchEditorEndpoint}?fileName=${window.encodeURIComponent(
      errorLocation.fileName
    )}&lineNumber=${window.encodeURIComponent(errorLocation.lineNumber || 1)}`
  );
});

hot.useCustomOverlay({
  showProblems(type, errors) {
    const formatted = formatWebpackMessages({
      errors,
      warnings: []
    });
    ErrorOverlay.reportBuildError(formatted.errors[0]);
  },
  clear() {
    ErrorOverlay.dismissBuildError();
  }
});

hot.setOptionsAndConnect({
  name: 'client',
  reload: true
});

ErrorOverlay.startReportingRuntimeErrors({
  filename: '/assets/client.js'
});

if (module.hot) {
  module.hot.dispose(ErrorOverlay.stopReportingRuntimeErrors);
}
