'use strict';

const fs = require('fs');
const path = require('path');
const glob = require('glob');
const mkdirp = require('mkdirp');
const rimraf = require('rimraf');

/**
 * FILE read
 */
function readFile(file) {
  return new Promise((resolve, reject) => {
    fs.readFile(file, 'utf8', (err, content) => {
      if (err) {
        reject(err);
      }
      resolve(content);
    });
  });
}

/**
 * FILE write
 */
function writeFile(file, content) {
  return new Promise((resolve, reject) => {
    fs.writeFile(file, content, 'utf8', err => {
      if (err) {
        reject(err);
      }
      resolve();
    });
  });
}

/**
 * FILE rename
 */
function renameFile(source, target) {
  return new Promise((resolve, reject) => {
    fs.rename(source, target, err => {
      if (err) {
        reject(err);
      }
      resolve();
    });
  });
}

/**
 * FILE move
 */
function moveFile(source, target) {
  return renameFile(source, target);
}

/**
 * FILE copy
 */
function copyFile(source, target) {
  return new Promise((resolve, reject) => {
    function done(err) {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    }

    const read = fs.createReadStream(source);
    read.on('error', err => done(err));
    const write = fs.createWriteStream(target);
    write.on('error', err => done(err));
    write.on('close', err => done(err));
    read.pipe(write);
  });
}

/**
 * DIRECTORY read
 */
function readDir(pattern, options) {
  return new Promise((resolve, reject) => {
    glob(pattern, options, (err, files) => {
      if (err) {
        reject(err);
      }
      resolve(files);
    });
  });
}

/**
 * DIRECTORY create
 */
function makeDir(dir) {
  return new Promise((resolve, reject) => {
    mkdirp(dir, err => {
      if (err) {
        reject(err);
      }
      resolve();
    });
  });
}

/**
 * DIRECTORY move
 */
async function moveDir(source, target) {
  const dir = await readDir('**/*.*', {
    cwd: source,
    nosort: true,
    dot: true
  });
  return Promise.all(
    dir.map(async chunk => {
      const from = path.resolve(source, chunk);
      const to = path.resolve(target, chunk);
      await makeDir(path.dirname(to));
      await renameFile(from, to);
    })
  );
}

/**
 * DIRECTORY copy
 */
async function copyDir(source, target) {
  const dir = await readDir('**/*.*', {
    cwd: source,
    nosort: true,
    dot: true
  });
  return Promise.all(
    dir.map(async chunk => {
      const from = path.resolve(source, chunk);
      const to = path.resolve(target, chunk);
      await makeDir(path.dirname(to));
      await copyFile(from, to);
    })
  );
}

/**
 * DIRECTORY clean
 */
function cleanDir(pattern, options) {
  return new Promise((resolve, reject) => {
    rimraf(pattern, { glob: options }, err => {
      if (err) {
        reject(err);
      }
      resolve();
    });
  });
}

module.exports = {
  readFile,
  writeFile,
  renameFile,
  moveFile,
  copyFile,
  readDir,
  makeDir,
  moveDir,
  copyDir,
  cleanDir
};
