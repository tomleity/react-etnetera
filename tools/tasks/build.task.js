'use strict';

const taskRunner = require('../taskRunner');

const cleanTask = require('./clean.task');
const copyTask = require('./copy.task');
const bundleTask = require('./bundle.task');

if (
  process.argv.includes('--release') &&
  process.env.NODE_ENV !== 'production'
) {
  process.env.NODE_ENV = 'production';
}

async function build() {
  await taskRunner(cleanTask);
  await taskRunner(copyTask);
  await taskRunner(bundleTask);
}

module.exports = build;
