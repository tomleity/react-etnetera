'use strict';

/* eslint global-require: 0 */
/* eslint import/no-unresolved: 0 */

process.env.NODE_ENV = 'development';

require('../config/env');

const chalk = require('chalk').default;
const express = require('express');
const browserSync = require('browser-sync');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const errorOverlayMiddleware = require('react-dev-utils/errorOverlayMiddleware');
const webpackConfig = require('../../webpack.config');
const taskRunner = require('../taskRunner');
const cleanTask = require('./clean.task');
const paths = require('../config/paths');
const timer = require('../utils/timer');

const watchOptions = {
  // aggregateTimeout: 300,
  // poll: 1000,
  ignored: /node_modules/
};

/**
 * Patch config for development options
 */
function patchConfig(config) {
  const patched = { ...config };
  if (patched.name === 'client') {
    patched.entry.client = ['./tools/utils/webpack-hot-dev-client']
      .concat(config.entry.client)
      .sort((a, b) => b.includes('polyfill') - a.includes('polyfill'));
    patched.output.filename = config.output.filename.replace(
      'chunkhash',
      'hash'
    );
    patched.output.chunkFilename = config.output.chunkFilename.replace(
      'chunkhash',
      'hash'
    );
  }

  if (patched.name === 'server') {
    // patched.output.hotUpdateMainFilename = 'updates/[hash].hot-update.json';
    // patched.output.hotUpdateChunkFilename = 'updates/[id].[hash].hot-update.js';
  }

  config.plugins.push(
    new webpack.HotModuleReplacementPlugin({
      multiStep: false,
      fullBuildTimeout: 200,
      requestTimeout: 10000
    })
  );
  return config;
}

/**
 * Register compilation hooks
 */
function regsiterCompilationHooks(config, compiler) {
  const { name } = config;
  const nameColorized = chalk.blue(name);

  // HOOK - compilation
  compiler.hooks.compile.tap(name, () => {
    console.info(
      `[${taskRunner.timeFormat(
        timer.start(`${name}Compilation`)
      )}] Compiling '${nameColorized}' ...`
    );
  });

  // HOOK - finish
  compiler.hooks.done.tap(name, stats => {
    console.info(stats.toString(config.stats));
    if (stats.hasErrors()) {
      console.error(
        `[${taskRunner.timeFormat(
          timer.now()
        )}] Failed to compile '${nameColorized}' after ${timer.end(
          `${name}Compilation`
        )} ms`
      );
    } else {
      console.info(
        `[${taskRunner.timeFormat(
          timer.now()
        )}] Finished '${nameColorized}' after ${timer.end(
          `${name}Compilation`
        )} ms`
      );
    }
  });
}

/**
 * Development server task
 */
async function devServer() {
  await taskRunner(cleanTask);

  // const port = process.env.APP_PORT;
  // const host = process.env.APP_HOST;

  const clientConfig = patchConfig({ ...webpackConfig[0] });
  const clientCompiler = webpack(clientConfig);
  regsiterCompilationHooks(clientConfig, clientCompiler);

  const serverConfig = patchConfig({ ...webpackConfig[1] });
  const serverCompiler = webpack(serverConfig);

  // DEV_SERVER instance
  const server = express();

  // DEV_SERVER client
  server.use(errorOverlayMiddleware());
  server.use(express.static(paths.public));
  server.use(
    webpackDevMiddleware(clientCompiler, {
      publicPath: clientConfig.output.publicPath,
      logLevel: 'silent',
      writeToDisk: false,
      watchOptions
    })
  );
  server.use(
    webpackHotMiddleware(clientCompiler, {
      log: false
    })
  );

  let app;

  // DEV_SERVER server
  serverCompiler.run((err, stats) => {
    if (!err && !stats.hasErrors()) {
      app = require('../../build/server').default;
    }
  });
  server.use((req, res) => {
    app.handle(req, res);
  });

  // DEV_SERVER browser-sync
  await new Promise((resolve, reject) => {
    browserSync.create('dev').init(
      {
        server: 'src/server/index.js',
        middleware: [server],
        open: false
      },
      (error, bs) => (error ? reject(error) : resolve(bs))
    );
  });
}

module.exports = devServer;
