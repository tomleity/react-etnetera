'use strict';

const webpack = require('webpack');
const webpackConfig = require('../../webpack.config.js');

function bundle() {
  return new Promise((resolve, reject) => {
    webpack(webpackConfig, (err, stats) => {
      if (err) {
        reject(err);
      }
      console.info(stats.toString(webpackConfig[0].stats));
      return resolve();
    });
  });
}

module.exports = bundle;
