'use strict';

/* eslint global-require: 0 */
/* eslint import/no-unresolved: 0 */

process.env.NODE_ENV = 'production';

require('../config/env');

// const fs = require('../utils/fs');
// const paths = require('../config/paths');
const taskRunner = require('../taskRunner');
const buildTask = require('./build.task');

/**
 * Production server task
 */
async function server() {
  // await fs
  //   .readDir('**/*.*', {
  //     cwd: paths.build,
  //     nosort: true,
  //     dot: true
  //   })
  //   .then(async files => {
  //     if (
  //       !files.includes('public/index.html') ||
  //       !files.includes('server.js')
  //     ) {
  //       await taskRunner(buildTask);
  //     }
  //   });

  await taskRunner(buildTask);

  return new Promise((resolve, reject) => {
    try {
      require('../../build/server');
      resolve();
    } catch (err) {
      reject(err);
    }
  });
}

module.exports = server;
