'use strict';

const path = require('path');
const fs = require('../utils/fs');
const paths = require('../config/paths');

const IGNORE = [];

/**
 * Clean build directory task
 */
function clean() {
  return fs.cleanDir(path.join(paths.build, '*'), {
    nosort: true,
    dot: true,
    ignore: IGNORE.map(file => path.join(paths.build, file))
  });
}

module.exports = clean;
