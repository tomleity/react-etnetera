'use strict';

const fs = require('../utils/fs');
const paths = require('../config/paths');
const pkg = require('../../package.json');

const DEV_MODE =
  !process.argv.includes('--release') && process.env.NODE_ENV !== 'production';

/**
 * Copy files task
 */
async function copy() {
  await fs.makeDir(paths.build);
  await Promise.all([
    fs.copyDir(paths.public, paths.buildPublic),
    fs.writeFile(
      paths.resolve(paths.build, 'package.json'),
      JSON.stringify(
        {
          private: true,
          engines: pkg.engines,
          dependencies: pkg.dependencies,
          scripts: {
            start: 'node server.js'
          }
        },
        null,
        2
      )
    ),
    fs.copyFile(
      paths.resolve('package-lock.json'),
      paths.resolve(paths.build, 'package-lock.json')
    ),
    fs.copyFile(
      paths.resolve('LICENSE.txt'),
      paths.resolve(paths.build, 'LICENSE.txt')
    ),
    fs.copyFile(
      paths.resolve(`.env.${DEV_MODE ? 'development' : 'production'}`),
      paths.resolve(paths.build, '.env.settings')
    )
  ]);
}

module.exports = copy;
