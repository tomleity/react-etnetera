/* eslint global-require: 0 */
/* eslint import/no-dynamic-require: 0 */

const chalk = require('chalk').default;
const timer = require('./utils/timer');

/**
 * Format time
 */
function format(date) {
  return date.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, '$1');
}

/**
 * Task runner
 */
function runner(fn, options) {
  const task = typeof fn.default === 'undefined' ? fn : fn.default;
  const taskInfo = `${chalk.bold.blue(task.name)}${
    options ? ` (${options})` : ''
  }`;

  console.info(
    `[${format(timer.start(task.name))}] Starting '${taskInfo}' ...`
  );

  const run = task(options).then(response => {
    console.info(
      `[${format(timer.now())}] Finished '${taskInfo}' after ${timer.end(
        task.name
      )} ms`
    );
    return response;
  });

  if (require.main !== module) {
    run.catch(err => console.error(chalk.red(err.stack || err)));
  }
  return run;
}

/**
 * CLI support
 */
if (require.main === module && process.argv[2]) {
  const file = __filename;
  delete require.cache[file];

  const module = require(`./tasks/${process.argv[2]}.task`);
  runner(module).catch(err => {
    console.error(chalk.red(err.stack || err));
    process.exit(1);
  });
}

module.exports = runner;
module.exports.timeFormat = format;
