'use strict';

/* eslint global-require: 0 */

const fs = require('fs');
const paths = require('./paths');

delete require.cache[require.resolve('./paths')];

let { NODE_ENV = 'development' } = process.env;
if (process.argv.includes('--release')) {
  NODE_ENV = 'production';
}
if (!NODE_ENV) {
  throw new Error(
    'The NODE_ENV environment variable is required but was not specified.'
  );
}

/**
 * Enviroment files
 */
const dotenvFiles = [
  `${paths.dotenv}.${NODE_ENV}.local`,
  `${paths.dotenv}.${NODE_ENV}`,

  NODE_ENV !== 'test' && `${paths.dotenv}.local`,

  paths.dotenv
].filter(Boolean);

dotenvFiles.forEach(dotenvFile => {
  if (fs.existsSync(dotenvFile)) {
    require('dotenv-expand')(
      require('dotenv').config({
        path: dotenvFile
      })
    );
  }
});

const PREFIX = /^APP_/i;

/**
 * Enviroment variables
 */
function getClientEnvironment(publicPath) {
  // Raw enviroment variables
  const raw = Object.keys(process.env)
    .filter(key => PREFIX.test(key))
    .reduce(
      (env, key) => {
        const arr = env;
        arr[key] = process.env[key];
        return arr;
      },
      {
        NODE_ENV: NODE_ENV || 'development',
        PUBLIC_PATH: publicPath
      }
    );

  // Stringified eviroment variables
  const stringified = {
    'process.env': Object.keys(raw).reduce((env, key) => {
      const arr = env;
      arr[key] = JSON.stringify(raw[key]);
      return arr;
    }, {})
  };

  return { raw, stringified };
}

module.exports = getClientEnvironment;
