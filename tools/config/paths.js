'use strict';

const path = require('path');
const url = require('url');

/**
 * Resolve paths from root directory
 */
function resolve(...args) {
  return path.resolve(path.resolve(__dirname, '../../'), ...args);
}

/**
 * Ensure slash in the end of the given pathname
 */
function ensureSlash(pathname, required = false) {
  const hasSlash = pathname.endsWith('/');
  if (hasSlash && !required) {
    return pathname.substr(pathname, pathname.length - 1);
  }
  if (!hasSlash && required) {
    return `${pathname}/`;
  }
  return pathname;
}

/**
 * Get public path
 */
function getPublicPath() {
  return process.env.APP_PUBLIC_PATH || '/';
}

/**
 * Get served path
 */
function getServedPath() {
  const publicPath = getPublicPath();
  return ensureSlash(publicPath ? url.parse(publicPath).pathname : '/', false);
}

module.exports = {
  resolve,
  root: resolve(),
  dotenv: resolve('.env'),
  src: resolve('src'),
  build: resolve('build'),
  buildPublic: resolve('build/public'),
  public: resolve('public'),
  publicHtml: resolve('public/index.html'),
  client: resolve('src/client'),
  server: resolve('src/server'),
  publicPath: getPublicPath,
  servedPath: getServedPath
};
