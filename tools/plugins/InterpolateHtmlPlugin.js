'use strict';

const escapeStringRegexp = require('escape-string-regexp');

class InterpolateHtmlPlugin {
  constructor(replacements) {
    this.replacements = replacements;
  }

  apply(compiler) {
    compiler.hooks.compilation.tap('InterpolateHtmlPlugin', compilation => {
      compilation.hooks.htmlWebpackPluginBeforeHtmlProcessing.tapAsync(
        'InterpolateHtmlPlugin',
        (data, cb) => {
          const replaced = data;
          Object.keys(this.replacements).forEach(key => {
            const value = this.replacements[key];
            replaced.html = data.html.replace(
              new RegExp(`%${escapeStringRegexp(key)}%`, 'g'),
              value
            );
          });
          cb(null, replaced);
        }
      );
    });
  }
}

module.exports = InterpolateHtmlPlugin;
