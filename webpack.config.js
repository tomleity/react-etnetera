'use strict';

const fs = require('fs');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackAssetsManifest = require('webpack-assets-manifest');
const nodeExternals = require('webpack-node-externals');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const InterpolateHtmlPlugin = require('./tools/plugins/InterpolateHtmlPlugin');
const getClientEnviroment = require('./tools/config/env');
const paths = require('./tools/config/paths');
const pkg = require('./package.json');

const env = getClientEnviroment(paths.servedPath());

/**
 * Arguments
 */
const DEV_MODE = env.raw.NODE_ENV !== 'production';
const VERBOSE_MODE = process.argv.includes('--verbose');

/**
 * Exclude .map files from manifest
 */
function manifestCustomize(key, value) {
  if (key.toLowerCase().endsWith('.map')) {
    return false;
  }
  return { key, value };
}

/**
 * Generate chunk manifest
 */
function manifestDone(manifest, stats) {
  const chunkFileName = paths.resolve(paths.build, 'chunk-manifest.json');
  try {
    const filterHelper = file => !file.endsWith('.map');
    const pathHelper = file => manifest.getPublicPath(file);
    const chunkFiles = stats.compilation.chunkGroups.reduce((result, group) => {
      const chunks = [];
      chunks[group.name] = [
        ...(result[group.name] || []),
        ...group.chunks.reduce(
          (files, chunk) => [
            ...files,
            ...chunk.files.filter(filterHelper).map(pathHelper)
          ],
          []
        )
      ];
      return chunks;
    }, Object.create(null));
    fs.writeFileSync(chunkFileName, JSON.stringify(chunkFiles, null, 2));
  } catch (err) {
    console.error(`ERROR: Cannot write ${chunkFileName}: `, err);
    if (!DEV_MODE) {
      process.exit(1);
    }
  }
}

/**
 * WEBPACK client
 */
const client = {
  context: paths.root,

  name: 'client',
  target: 'web',
  mode: DEV_MODE ? 'development' : 'production',

  bail: !DEV_MODE,
  cache: DEV_MODE,

  entry: {
    client: [
      require.resolve('@babel/polyfill'),
      require.resolve('./src/client/index.js')
    ]
  },

  output: {
    pathinfo: VERBOSE_MODE,
    publicPath: paths.publicPath(),
    path: paths.buildPublic,
    filename: DEV_MODE ? 'static/[name].js' : 'static/[name].[chunkhash:12].js',
    chunkFilename: DEV_MODE
      ? 'static/[name].chunk.js'
      : 'static/[name].[chunkhash:12].chunk.js',

    // devtoolModuleFilenameTemplate: info => path.resolve(info.absoluteResourcePath).replace(/\\/g, '/'),
    devtoolModuleFilenameTemplate: 'file://[absolute-resource-path]',
    devtoolFallbackModuleFilenameTemplate:
      'file://[absolute-resource-path]?[hash]'
  },

  resolve: {
    modules: ['node_modules', paths.src],
    extensions: ['.js', '.jsx', '.json'],
    alias: {}
  },

  module: {
    strictExportPresence: true,
    rules: [
      {
        parser: {
          requireEnsure: false
        }
      },
      {
        test: /\.(js|jsx)$/,
        include: paths.client,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        options: {
          babelrc: false,
          cacheDirectory: DEV_MODE,
          presets: [
            [
              '@babel/preset-env',
              {
                targets: {
                  browsers: pkg.browserslist
                },
                debug: false,
                modules: false,
                useBuiltIns: false,
                forceAllTransforms: !DEV_MODE
              }
            ],
            '@babel/preset-flow',
            ['@babel/preset-react', { development: DEV_MODE }]
          ],
          plugins: [
            '@babel/plugin-syntax-dynamic-import',
            '@babel/plugin-proposal-class-properties',
            ...(DEV_MODE
              ? []
              : [
                  '@babel/transform-react-constant-elements',
                  '@babel/transform-react-inline-elements',
                  [
                    'transform-react-remove-prop-types',
                    {
                      removeImport: true,
                      ignoreFilenames: ['node_modules'],
                      mode: 'remove'
                    }
                  ]
                ])
          ]
        }
      },
      {
        test: /\.(css|sass|scss)$/,
        exclude: /node_modules/,
        include: paths.client,
        use: [
          {
            loader: DEV_MODE ? 'style-loader' : MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              // CSS Loader
              importLoaders: 2,
              sourceMap: DEV_MODE,
              // CSS Modules
              modules: true,
              localIdentName: DEV_MODE
                ? '[name]-[local]-[hash:base64:12]'
                : '[hash:base64:12]'
            }
          },
          { loader: 'postcss-loader' },
          { loader: 'sass-loader' },
          {
            loader: 'sass-resources-loader',
            options: {
              resources: [
                paths.resolve('src/client/assets/scss/variables.scss'),
                paths.resolve('src/client/assets/scss/mixins.scss')
              ]
            }
          }
        ]
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'svg-sprite-loader',
            options: { extract: false }
          },
          {
            loader: 'svgo-loader',
            options: {
              plugins: [
                { removeTitle: true },
                { removeUnknownsAndDefaults: true },
                { removeUselessStrokeAndFill: { stroke: true, fill: true } }
              ]
            }
          }
        ]
      },
      {
        test: /\.(bmp|gif|jpg|jpeg|png)$/,
        loader: 'file-loader',
        options: {
          name: DEV_MODE
            ? '[path][name].[ext]?[hash:8]'
            : 'static/images/[hash:8].[ext]'
        }
      },
      ...(DEV_MODE
        ? []
        : [
            {
              test: paths.resolve(
                'node_modules/react-deep-force-update/lib/index.js'
              ),
              loader: 'null-loader'
            }
          ])
    ]
  },

  plugins: [
    new webpack.DefinePlugin(env.stringified),
    new MiniCssExtractPlugin({
      filename: DEV_MODE
        ? 'static/[name].css'
        : 'static/[name].[contenthash:12].css'
    }),
    new HtmlWebpackPlugin({
      inject: true,
      filename: 'index.html',
      template: paths.resolve(paths.client, 'index.html')
    }),
    new InterpolateHtmlPlugin(env.raw),
    new WebpackAssetsManifest({
      output: paths.resolve(paths.build, 'asset-manifest.json'),
      publicPath: true,
      writeToDisk: true,
      customize: ({ key, value }) => manifestCustomize(key, value),
      done: (manifest, stats) => manifestDone(manifest, stats)
    })
  ],

  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: false
      }),
      new OptimizeCSSAssetsPlugin({})
    ],
    splitChunks: {
      cacheGroups: {
        commons: {
          name: 'vendors',
          chunks: 'initial',
          test: /[\\/]node_modules[\\/]/
        },
        styles: {
          name: 'styles',
          test: /\.css$/,
          chunks: 'all',
          enforce: true
        }
      }
    }
  },

  stats: {
    cached: VERBOSE_MODE,
    cachedAssets: VERBOSE_MODE,
    chunks: VERBOSE_MODE,
    chunkModules: VERBOSE_MODE,
    colors: true,
    hash: VERBOSE_MODE,
    modules: VERBOSE_MODE,
    reasons: DEV_MODE,
    timings: true,
    version: VERBOSE_MODE
  },

  node: {
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty'
  },

  devtool: DEV_MODE ? 'eval-source-map' : 'source-map'
};

/**
 * WEBPACK server
 */
const server = {
  context: paths.root,

  name: 'server',
  target: 'node',
  mode: DEV_MODE ? 'development' : 'production',

  bail: !DEV_MODE,
  cache: DEV_MODE,

  entry: {
    server: [
      require.resolve('@babel/polyfill'),
      require.resolve('./src/server/index.js')
    ]
  },

  output: {
    path: paths.build,
    pathinfo: VERBOSE_MODE,
    filename: '[name].js',
    library: 'server',
    libraryTarget: 'commonjs2',

    // devtoolModuleFilenameTemplate: info => path.resolve(info.absoluteResourcePath).replace(/\\/g, '/'),
    devtoolModuleFilenameTemplate: 'file://[absolute-resource-path]',
    devtoolFallbackModuleFilenameTemplate:
      'file://[absolute-resource-path]?[hash]'
  },

  externals: [
    './chunk-manifest.json',
    './asset-manifest.json',
    nodeExternals({
      whitelist: []
    })
  ],

  module: {
    strictExportPresence: true,
    rules: [
      {
        parser: {
          requireEnsure: false
        }
      },
      {
        test: /\.(js)$/,
        include: paths.server,
        exclude: /(node_modules)/,
        loader: require.resolve('babel-loader'),
        options: {
          babelrc: false,
          cacheDirectory: DEV_MODE,
          presets: [
            [
              '@babel/preset-env',
              {
                targets: {
                  node: pkg.engines.node.match(/(\d+\.?)+/)[0]
                },
                debug: false,
                modules: false,
                useBuiltIns: false,
                forceAllTransforms: !DEV_MODE
              }
            ],
            '@babel/preset-flow'
          ],
          plugins: [
            '@babel/plugin-syntax-dynamic-import',
            '@babel/plugin-proposal-class-properties'
          ]
        }
      }
    ]
  },

  plugins: [new webpack.DefinePlugin(env.stringified)],

  stats: {
    cached: VERBOSE_MODE,
    cachedAssets: VERBOSE_MODE,
    chunks: VERBOSE_MODE,
    chunkModules: VERBOSE_MODE,
    colors: true,
    hash: VERBOSE_MODE,
    modules: VERBOSE_MODE,
    reasons: DEV_MODE,
    timings: true,
    version: VERBOSE_MODE
  },

  node: {
    console: false,
    global: false,
    process: false,
    Buffer: false,
    __filename: false,
    __dirname: false
  }

  // devtool: DEV_MODE ? 'eval-source-map' : 'source-map'
};

module.exports = [client, server];
