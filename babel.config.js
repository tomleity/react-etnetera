'use strict';

const pkg = require('./package.json');

module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          node: pkg.engines.node.match(/(\d+\.?)+/)[0]
        },
        modules: 'commonjs',
        useBuiltIns: false,
        debug: false
      }
    ],
    '@babel/preset-flow',
    '@babel/preset-react'
  ],
  plugins: [
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-proposal-class-properties'
  ],
  ignore: ['node_modules', 'build']
};
