'use strict';

import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ component: Component, user, access, ...rest }) => {
  const check = user.active && (access ? access.includes(user.type) : true);

  return (
    <Route
      {...rest}
      render={props =>
        check ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: '/', state: { from: props.location } }} />
        )
      }
    />
  );
};

export default PrivateRoute;
