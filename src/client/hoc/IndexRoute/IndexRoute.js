'use strict';

import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ component: Component, user, access, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      !user.active ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{ pathname: '/devices', state: { from: props.location } }}
        />
      )
    }
  />
);

export default PrivateRoute;
