'use strict';

// @flow
import { type Node } from 'react';

type Props = {
  children: Node
};

const AuxWrapper = (props: Props) => props.children;

export default AuxWrapper;
