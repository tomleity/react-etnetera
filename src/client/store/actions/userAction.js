'use strict';

import actionTypes from './userTypes';

const login = userData => ({
  type: actionTypes.USER_LOGIN,
  user: userData
});

const logout = () => ({
  type: actionTypes.USER_LOGOUT
});

export default { login, logout };
