'use strict';

import actionTypes from './appTypes';

const setHeaderTheme = theme => ({
  type: actionTypes.APP_THEME_HEADER,
  header: theme
});

export default { setHeaderTheme };
