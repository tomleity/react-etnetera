'use strict';

export const updateState = (oldState, updatedState) =>
  Object.assign({}, oldState, updatedState);
