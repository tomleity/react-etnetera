'use strict';

import actionTypes from '../actions/userTypes';
import { updateState } from '../utils';

const initialState = () => {
  let user = localStorage.getItem('user');
  if (user) {
    user = JSON.parse(user);
    return Object.assign({}, { active: 1 }, user);
  }
  return {
    active: 0,
    id: '',
    type: '',
    login: '',
    name: '',
    token: ''
  };
};

const login = (state, action) => {
  const updatedUser = {
    active: 1,
    id: action.user.id,
    type: action.user.type,
    login: action.user.login,
    name: action.user.name,
    token: action.user.token
  };
  localStorage.setItem('user', JSON.stringify(updatedUser));
  return updateState(state, updatedUser);
};

const logout = () => {
  localStorage.removeItem('user');
  return initialState;
};

const reducer = (state = initialState(), action) => {
  switch (action.type) {
    case actionTypes.USER_LOGIN:
      return login(state, action);
    case actionTypes.USER_LOGOUT:
      return logout(state, action);
    default:
      return state;
  }
};

export default reducer;
