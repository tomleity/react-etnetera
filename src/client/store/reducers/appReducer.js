'use strict';

import actionTypes from '../actions/appTypes';
import { updateState } from '../utils';

const initialState = () => ({
  theme: {
    header: {
      solid: true
    }
  }
});

const setHeaderTheme = (state, action) =>
  updateState(state, {
    theme: {
      header: action.header
    }
  });

const reducer = (state = initialState(), action) => {
  switch (action.type) {
    case actionTypes.APP_THEME_HEADER:
      return setHeaderTheme(state, action);
    default:
      return state;
  }
};

export default reducer;
