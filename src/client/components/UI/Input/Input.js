'use strict';

// @flow
import React, { type Node } from 'react';
import styles from './Input.scss';

type Props = {
  id: string,
  elementType: string,
  elementConfig?: { [param: string]: any },
  value: string,
  label?: string,
  valid?: boolean,
  touched?: boolean,
  inline?: boolean,
  className?: {
    field?: string,
    label?: string,
    input?: string,
    message?: string
  },
  handleChange: (SyntheticInputEvent<HTMLInputElement>) => mixed
};

const Input = (props: Props): Node => {
  const {
    id,
    elementType,
    elementConfig,
    value,
    label,
    valid,
    touched,
    inline,
    className,
    handleChange
  } = props;

  const classesList = {
    field: [styles.field, inline ? styles.inline : '', className.field],
    label: [styles.inputLabel, className.label],
    input: [styles.inputElement, className.input],
    message: [styles.invalidMessage, className.message]
  };

  let invalidMessage = null;
  if (!valid && touched) {
    classesList.input.push(styles.invalid);
    if (!value) {
      invalidMessage = 'Je nutno vyplnit';
    } else {
      invalidMessage = 'Neplatný parameter';
    }
  }

  let inputElement = null;
  switch (elementType) {
    case 'input':
      inputElement = (
        <input
          id={id}
          {...elementConfig}
          value={value}
          className={classesList.input.join(' ')}
          onChange={handleChange}
        />
      );
      break;
    case 'textarea':
      inputElement = (
        <textarea
          id={id}
          {...elementConfig}
          className={classesList.input.join(' ')}
          onChange={handleChange}
        >
          {value}
        </textarea>
      );
      break;
    case 'select':
      inputElement = (
        <select
          id={id}
          value={value}
          className={classesList.input.join(' ')}
          onChange={handleChange}
        >
          {elementConfig.options.map(option => (
            <option key={option.value} value={option.value}>
              {option.displayValue}
            </option>
          ))}
        </select>
      );
      break;
    default:
      inputElement = (
        <input
          id={id}
          {...elementConfig}
          value={value}
          className={classesList.input.join(' ')}
          onChange={handleChange}
        />
      );
  }

  return (
    <div className={classesList.field.join(' ')}>
      {label ? (
        <label htmlFor={id} className={classesList.label.join(' ')}>
          {label}
        </label>
      ) : null}
      {inputElement}
      {invalidMessage ? (
        <p className={classesList.message.join(' ')}>*{invalidMessage}</p>
      ) : null}
    </div>
  );
};

Input.defaultProps = {
  elementConfig: {},
  label: '',
  valid: true,
  touched: false,
  inline: false,
  className: {
    field: '',
    label: '',
    input: '',
    message: ''
  }
};

export default Input;
