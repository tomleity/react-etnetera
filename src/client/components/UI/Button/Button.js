'use strict';

/* eslint react/button-has-type: 0 */

// @flow
import React, { type Node } from 'react';
import cn from 'classnames';
import styles from './Shared.scss';

type ButtonType = 'button' | 'submit' | 'reset';
type ButtonStyle = 'default' | 'info' | 'success' | 'error';

type Props = {
  type?: ButtonType,
  btnStyle?: ButtonStyle,
  disabled?: boolean,
  className?: string,
  clicked?: (SyntheticEvent<HTMLButtonElement>) => mixed,
  children: Node
};

const Button = (props: Props): Node => {
  const { type, btnStyle, disabled, className, clicked, children } = props;

  return (
    <button
      type={type}
      disabled={disabled}
      className={cn(styles.button, styles[btnStyle], className)}
      onClick={type === 'button' ? clicked : undefined}
    >
      {children}
    </button>
  );
};

Button.defaultProps = {
  type: 'button',
  btnStyle: 'info',
  disabled: false,
  className: '',
  clicked: event => console.info(event.currentTarget)
};

export default Button;
