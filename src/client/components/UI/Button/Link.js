'use strict';

/* eslint react/button-has-type: 0 */

// @flow
import React, { type Node } from 'react';
import { Link } from 'react-router-dom';
import cn from 'classnames';
import styles from './Shared.scss';

type ButtonStyle = 'default' | 'info' | 'success' | 'error';

type Props = {
  to: string | {},
  replace?: boolean,
  btnStyle?: ButtonStyle,
  className?: string,
  children: Node
};

const LinkButton = (props: Props): Node => {
  const { to, replace, btnStyle, className, children } = props;

  return (
    <Link
      replace={replace}
      to={to}
      className={cn(styles.button, styles[btnStyle], className)}
    >
      {children}
    </Link>
  );
};

LinkButton.defaultProps = {
  replace: false,
  btnStyle: 'info',
  className: ''
};

export default LinkButton;
