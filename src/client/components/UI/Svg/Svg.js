'use strict';

// @flow
import React, { type Node } from 'react';
import glyphs from '../../../helpers/svg';
import styles from './Svg.scss';

type Props = {
  name: string,
  style?: { [string]: mixed },
  className?: string
};

const Svg = (props: Props): Node => {
  const { name, style, className = '' } = props;
  const { viewBox } = glyphs[name].default;
  let c = `${styles.svg}`;

  if (c) {
    c += ` ${className}`;
  }

  return (
    <svg className={c} style={style} viewBox={viewBox}>
      <use xlinkHref={`#${name}`} />
    </svg>
  );
};

Svg.defaultProps = {
  style: {},
  className: ''
};

export default Svg;
