'use strict';

// @flow
import React, { type Node } from 'react';
import { connect } from 'react-redux';
import { Router, Route, Switch } from 'react-router-dom';
import PrivateRoute from '../hoc/PrivateRoute/PrivateRoute';
import IndexRoute from '../hoc/IndexRoute/IndexRoute';
import history from '../helpers/history';

import LoginRoute from '../routes/Login/LoginContainer';
import DevicesRoute from '../routes/Devices/DevicesContainer';
import ManageRoute from '../routes/Manage/ManageContainer';
import ManageCreateRoute from '../routes/ManageCreate/ManageCreate';
import ManageUpdateRoute from '../routes/ManageUpdate/ManageUpdate';
import NoMatch from '../routes/NoMatch/NoMatch';

import './App.scss';

type Props = {
  user: {
    active: boolean,
    type: string
  }
};

class App extends React.Component<Props> {
  render(): Node {
    const { user } = this.props;

    return (
      <Router history={history}>
        <Switch>
          <IndexRoute exact path="/" component={LoginRoute} user={user} />
          <PrivateRoute
            exact
            path="/devices"
            component={DevicesRoute}
            user={user}
          />
          <PrivateRoute
            exact
            path="/manage"
            component={ManageRoute}
            user={user}
            access={['admin']}
          />
          <PrivateRoute
            exact
            path="/manage/create"
            component={ManageCreateRoute}
            user={user}
            access={['admin']}
          />
          <PrivateRoute
            exact
            path="/manage/:id/update"
            component={ManageUpdateRoute}
            user={user}
            access={['admin']}
          />
          <Route component={NoMatch} />
        </Switch>
      </Router>
    );
  }
}

const mapStateToProps = state => ({
  user: {
    active: state.user.active,
    type: state.user.type
  }
});

export default connect(mapStateToProps)(App);
