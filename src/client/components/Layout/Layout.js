'use strict';

// @flow
import React, { type Node } from 'react';
import AuxWrapper from '../../hoc/AuxWrapper/AuxWrapper';
import Header from '../Header/HeaderContainer';
import styles from './Layout.scss';

type Props = {
  children: Node,
  header?: {
    solid?: boolean
  }
};

class Layout extends React.Component<Props> {
  static defaultProps = {
    header: {
      solid: true
    }
  };

  render() {
    const { children, header } = this.props;
    return (
      <AuxWrapper>
        <Header header={header} />
        <main className={styles.content}>{children}</main>
      </AuxWrapper>
    );
  }
}

export default Layout;
