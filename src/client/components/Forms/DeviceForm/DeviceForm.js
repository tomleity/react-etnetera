'use strict';

// @flow
import React, { type Node } from 'react';
import Input from '../../UI/Input/Input';
import Button from '../../UI/Button/Button';
import styles from './DeviceForm.scss';

type Props = {
  validated?: boolean,
  defaults?: {
    code?: string,
    vendor?: string,
    model?: string,
    os?: string,
    osVersion?: string,
    image?: string
  },
  submitHandler: (SyntheticEvent<HTMLFormElement>) => any
};

type State = {
  deviceForm: {
    elements: { [x: string]: any },
    valid: boolean,
    error: string
  }
};

class DeviceForm extends React.Component<Props, State> {
  static defaultProps = {
    validated: false,
    defaults: {
      code: '',
      vendor: '',
      model: '',
      os: '',
      osVersion: '',
      image: ''
    }
  };

  constructor(props: Props) {
    super(props);
    this.state = {
      deviceForm: {
        elements: {
          code: {
            elementType: 'input',
            elementConfig: {
              type: 'text',
              placeholder: 'ETN-M-[number]'
            },
            value: props.defaults.code,
            label: 'Kódové označení (identifikátor)',
            validation: {
              required: true
            },
            valid: props.validated,
            touched: false
          },
          vendor: {
            elementType: 'input',
            elementConfig: {
              type: 'text',
              placeholder: 'SAMSUNG'
            },
            value: props.defaults.vendor,
            label: 'Výrobce',
            validation: {
              required: true
            },
            valid: props.validated,
            touched: false
          },
          model: {
            elementType: 'input',
            elementConfig: {
              type: 'text',
              placeholder: 'Galaxy S3'
            },
            value: props.defaults.model,
            label: 'Model',
            validation: {
              required: true
            },
            valid: props.validated,
            touched: false
          },
          os: {
            elementType: 'input',
            elementConfig: {
              type: 'text',
              placeholder: 'ANDROID'
            },
            value: props.defaults.os,
            label: 'Operační systém',
            validation: {
              required: true
            },
            valid: props.validated,
            touched: false
          },
          osVersion: {
            elementType: 'input',
            elementConfig: {
              type: 'text',
              placeholder: '8.1'
            },
            value: props.defaults.osVersion,
            label: 'Verze operačního systému',
            validation: {
              required: false
            },
            valid: true,
            touched: false
          },
          image: {
            elementType: 'input',
            elementConfig: {
              type: 'text',
              placeholder: 'https://www.example.com/samsung-galaxy-s3.jpg'
            },
            value: props.defaults.image,
            label: 'Obrázek (URL)',
            validation: {
              required: false
              // isUrl: true
            },
            valid: true,
            touched: false
          }
        },
        valid: props.validated,
        error: ''
      }
    };

    this.handleChange = this.handleChange.bind(this);
  }

  validation = (value, rules) => {
    let isValid = true;
    if (!rules) {
      return isValid;
    }

    if (rules.required) {
      isValid = value.trim() !== '' && isValid;
    }

    // if (rules.isUrl) {
    //   isValid = value.startsWith('www') || value.startsWith('http');
    // }

    return isValid;
  };

  handleChange = (event, id) => {
    const { deviceForm } = this.state;

    const updatedDeviceForm = Object.assign({}, deviceForm);
    const updatedFormElement = Object.assign(
      {},
      updatedDeviceForm.elements[id]
    );

    updatedFormElement.value = event.currentTarget.value;
    updatedFormElement.valid = this.validation(
      updatedFormElement.value,
      updatedFormElement.validation
    );
    updatedFormElement.touched = true;
    updatedDeviceForm.elements[id] = updatedFormElement;

    let formIsValid = true;
    Object.keys(updatedDeviceForm.elements).forEach(key => {
      formIsValid = updatedDeviceForm.elements[key].valid && formIsValid;
    });
    updatedDeviceForm.valid = formIsValid;

    this.setState({ deviceForm: updatedDeviceForm });
  };

  render(): Node {
    const { deviceForm } = this.state;
    const { validated, submitHandler } = this.props;

    const formElements = [];
    Object.keys(deviceForm.elements).forEach(key => {
      formElements.push({
        id: key,
        config: deviceForm.elements[key]
      });
    });

    return (
      <form onSubmit={event => submitHandler(event, deviceForm)}>
        {formElements.map(formElement => (
          <Input
            key={formElement.id}
            id={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            label={formElement.config.label}
            valid={formElement.config.valid}
            touched={formElement.config.touched}
            handleChange={event => this.handleChange(event, formElement.id)}
          />
        ))}
        <Button
          type="submit"
          disabled={!deviceForm.valid}
          className={styles.submitButton}
        >
          {!validated ? 'Vytvořit' : 'Aktualizovat'}
        </Button>
      </form>
    );
  }
}

export default DeviceForm;
