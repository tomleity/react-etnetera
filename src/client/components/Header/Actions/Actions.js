'use strict';

// @flow
import React, { type Node } from 'react';
import Link from '../../UI/Button/Link';
import Button from '../../UI/Button/Button';
import Svg from '../../UI/Svg/Svg';
import AuxWrapper from '../../../hoc/AuxWrapper/AuxWrapper';
import styles from './Actions.scss';

type Props = {
  user: {
    active: boolean,
    login: string,
    type: string
  },
  handleLogout: () => any
};

const Actions = (props: Props): Node => {
  const { user, handleLogout } = props;

  let items = null;
  if (user.active) {
    items = (
      <AuxWrapper>
        <h1 className={styles.username}>{user.name}</h1>
        <Button clicked={() => handleLogout()}>
          <span className={styles.buttonLong}>Odhlásit se</span>
          <span className={styles.buttonShort}>
            <Svg name="sign-out" className={styles.svgIcon} />
          </span>
        </Button>
        {user.type === 'admin' ? (
          <Link to="/manage" btnStyle="default" className={styles.manageLink}>
            <span className={styles.buttonLong}>Manage</span>
            <span className={styles.buttonShort}>
              <Svg name="cog" className={styles.svgIcon} />
            </span>
          </Link>
        ) : null}
      </AuxWrapper>
    );
  } else {
    items = (
      <Link replace to="/">
        Přihlásit
      </Link>
    );
  }

  return <div className={styles.actions}>{items}</div>;
};

export default Actions;
