'use strict';

// @flow
import React, { type Node } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import userAction from '../../store/actions/userAction';
import Header from './Header';

type Props = {
  header: {
    solid: boolean
  },
  user: {
    active: boolean,
    id: string,
    type: string,
    login: string,
    name: string
  },
  history: { [x: string]: any },
  location: { [x: string]: any },
  onLogout: () => any
};

class HeaderContainer extends React.Component<Props> {
  constructor(props) {
    super(props);

    this.handleLogout = this.handleLogout.bind(this);
  }

  handleLogout = () => {
    const { history, onLogout } = this.props;

    onLogout();
    history.push('/');
  };

  render(): Node {
    const { user, header, location } = this.props;
    return (
      <Header
        user={user}
        location={location}
        theme={header}
        handleLogout={this.handleLogout}
      />
    );
  }
}

const mapStateToProps = state => ({
  user: {
    active: state.user.active,
    id: state.user.id,
    type: state.user.type,
    login: state.user.login,
    name: state.user.name
  }
});

const mapDispatchToProps = dispatch => ({
  onLogout: () => dispatch(userAction.logout())
});

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(HeaderContainer);
