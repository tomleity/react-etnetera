'use strict';

/* eslint-disable */

// @flow
import React, { type Node } from 'react';
// import { NavLink } from 'react-router-dom';

import styles from './Navigation.scss';

type Props = {
  user: {
    type: string
  }
};

const Navigation = (props: Props): Node => {
  const { user } = props;
  return (
    <nav className={styles.nav}>
      {/* <ul className={styles.list}>
        <li className={styles.item} />
      </ul> */}
    </nav>
  );
};

export default Navigation;
