'use strict';

// @flow
import React, { type Node } from 'react';
import { Link } from 'react-router-dom';
import Svg from '../../UI/Svg/Svg';
import styles from './Brand.scss';

type Props = {
  location: { [x: string]: any }
};

const Brand = (props: Props): Node => {
  const { location } = props;

  return (
    <div className={styles.brand}>
      <div className={styles.logo}>
        <Link replace to={location.pathname}>
          <Svg name="logo" className={styles.logoSvg} />
        </Link>
      </div>
      <div className={styles.title}>
        <Link replace to={location.pathname} className={styles.titleLink}>
          <span className={styles.titleLong}>DeviceChecker</span>
          <span className={styles.titleShort}>DC</span>
        </Link>
      </div>
    </div>
  );
};

export default Brand;
