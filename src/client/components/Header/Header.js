'use strict';

// @flow
import React, { type Node } from 'react';
import Brand from './Brand/Brand';
import Actions from './Actions/Actions';
import Navigation from './Navigation/Navigation';
import styles from './Header.scss';

type Props = {
  theme: {
    solid: boolean
  },
  user: {
    active: boolean,
    id: string,
    type: string,
    login: string,
    name: string
  },
  location: { [x: string]: any },
  handleLogout: () => any
};

const Header = (props: Props): Node => {
  const { user, theme, location, handleLogout } = props;

  const classesList = [styles.header];
  if (!theme.solid) {
    classesList.push(styles.transparent);
  }

  return (
    <header className={classesList.join(' ')}>
      <Brand location={location} />
      <Navigation user={{ type: user.type }} />
      <Actions user={user} handleLogout={handleLogout} />
    </header>
  );
};

export default Header;
