'use strict';

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ApolloProvider } from 'react-apollo';
import deepForceUpdate from 'react-deep-force-update';

import client from './graphql/graphql';
import store from './store/store';
import App from './components/App';

const renderDOM = Container =>
  render(
    <ApolloProvider client={client}>
      <Provider store={store}>
        <Container />
      </Provider>
    </ApolloProvider>,
    document.getElementById('root')
  );

renderDOM(App);

if (module.hot) {
  module.hot.accept('./components/App', () => {
    deepForceUpdate(renderDOM(App));
  });
}
