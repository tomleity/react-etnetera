'use strict';

// @flow
import React, { type Node } from 'react';
import { Link } from 'react-router-dom';
import Svg from '../../../components/UI/Svg/Svg';
import styles from './ListSection.scss';

type Props = {
  devices: { [x: string]: any },
  handleDeviceDelete: (SyntheticEvent<HTMLButtonElement>, string) => any
};

const ListSection = (props: Props): Node => {
  const { devices, handleDeviceDelete } = props;
  return (
    <section className={styles.section}>
      <div className={styles.container}>
        {devices ? (
          <table className={styles.table}>
            <thead>
              <tr>
                <th scope="col">Kód zařízení</th>
                <th scope="col">Model</th>
                <th scope="col">Výrobce</th>
                <th scope="col">Operační systém</th>
                <th scope="col">Akce</th>
              </tr>
            </thead>
            <tbody>
              {devices.map(device => (
                <tr key={device.id}>
                  <td>#{device.code}</td>
                  <td>{device.model}</td>
                  <td>{device.vendor}</td>
                  <td>
                    {device.os}
                    {device.osVersion ? ` / ${device.osVersion}` : null}
                  </td>
                  <td>
                    <button
                      type="button"
                      className={styles.actionButton}
                      onClick={event => handleDeviceDelete(event, device.id)}
                    >
                      <Svg
                        name="times"
                        className={[styles.actionSvg, styles.svgDanger].join(
                          ' '
                        )}
                      />
                    </button>
                    <Link
                      to={`/manage/${device.id}/update`}
                      className={styles.actionLink}
                    >
                      <Svg
                        name="pen"
                        className={[styles.actionSvg, styles.svgInfo].join(' ')}
                      />
                    </Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : null}
      </div>
    </section>
  );
};

export default ListSection;
