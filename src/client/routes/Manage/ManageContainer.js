'use strict';

// @flow
import React, { type Node } from 'react';
import { graphql, compose } from 'react-apollo';
import { fetchPhones } from '../../graphql/queries/phoneQueries';
import { deletePhone } from '../../graphql/mutations/phoneMutations';
import Layout from '../../components/Layout/Layout';
import ActionsSection from './ActionsSection/ActionsSection';
import ListSection from './ListSection/ListSection';

type Props = {
  data: { [x: string]: any },
  deleteDevice: any => any
};

class ManageContainer extends React.Component<Props> {
  constructor(props) {
    super(props);

    this.handleDeviceDelete = this.handleDeviceDelete.bind(this);
  }

  handleDeviceDelete = (event, id) => {
    event.preventDefault();
    const { deleteDevice } = this.props;

    if (window.confirm(`Opravdu chcete odtranit zařízení? #ID: ${id}`)) {
      deleteDevice({
        variables: { id }
      });
    }
  };

  render(): Node {
    const { data } = this.props;

    return (
      <Layout>
        <ActionsSection />
        <ListSection
          devices={data.phones ? data.phones.data : []}
          handleDeviceDelete={this.handleDeviceDelete}
        />
      </Layout>
    );
  }
}

export default compose(
  graphql(fetchPhones),
  graphql(deletePhone, {
    name: 'deleteDevice',
    options: {
      refetchQueries: ['fetchPhones']
    }
  })
)(ManageContainer);
