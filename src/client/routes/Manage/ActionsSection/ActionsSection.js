'use strict';

// @flow
import React, { type Node } from 'react';
import Link from '../../../components/UI/Button/Link';
import styles from './ActionsSection.scss';

const ActionsSection = (): Node => (
  <section className={styles.section}>
    <div className={styles.container}>
      <div className={styles.actionsLeft}>
        <Link to="/devices" btnStyle="info" className={styles.link}>
          Zpět
        </Link>
      </div>
      <div className={styles.actionsRight}>
        <Link to="/manage/create" btnStyle="success" className={styles.link}>
          <span className={styles.buttonLong}>Nové zařízení</span>
          <span className={styles.buttonShort}>Nové</span>
        </Link>
      </div>
    </div>
  </section>
);

export default ActionsSection;
