'use strict';

// @flow
import React, { type Node } from 'react';
import { graphql, compose } from 'react-apollo';
import { fetchPhone } from '../../graphql/queries/phoneQueries';
import { updatePhone } from '../../graphql/mutations/phoneMutations';
import Layout from '../../components/Layout/Layout';
import DeviceForm from '../../components/Forms/DeviceForm/DeviceForm';
import styles from './ManageUpdate.scss';

type Props = {
  history: { [x: string]: any },
  match: { [x: string]: any },
  data: { [x: string]: any },
  update: any => any
};

class ManageUpdate extends React.Component<Props> {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit = (event, deviceForm) => {
    event.preventDefault();
    const { update, history, match } = this.props;

    const formData = {};
    Object.keys(deviceForm.elements).forEach(key => {
      formData[key] = deviceForm.elements[key].value;
    });

    update({
      variables: {
        id: match.params.id,
        data: formData
      }
    }).then(() => {
      history.push('/manage');
    });
  };

  render(): Node {
    const { data, match } = this.props;

    return (
      <Layout>
        <section className={styles.section}>
          <div className={styles.container}>
            <h1 className={styles.heading}>Zařízení: #{match.params.id}</h1>
            {data.phone ? (
              <DeviceForm
                validated
                defaults={{
                  code: data.phone.code,
                  vendor: data.phone.vendor,
                  model: data.phone.model,
                  os: data.phone.os,
                  osVersion: data.phone.osVersion,
                  image: data.phone.image
                }}
                submitHandler={this.handleSubmit}
              />
            ) : null}
          </div>
        </section>
      </Layout>
    );
  }
}

export default compose(
  graphql(fetchPhone, {
    options: props => ({
      variables: {
        id: props.match.params.id
      }
    })
  }),
  graphql(updatePhone, {
    name: 'update',
    options: {
      refetchQueries: ['fetchPhones']
    }
  })
)(ManageUpdate);
