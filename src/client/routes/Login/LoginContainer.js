'use strict';

// @flow
import React, { type Node } from 'react';
import { connect } from 'react-redux';
// $FlowFixMe
import { graphql, compose } from 'react-apollo';
import { loginMutation } from '../../graphql/mutations/userMutations';
import userAction from '../../store/actions/userAction';
import Layout from '../../components/Layout/Layout';
import LoginForm from './LoginForm/LoginForm';
import styles from './LoginContainer.scss';

type State = {
  loginForm: {
    elements: { [element: string]: any },
    valid: boolean,
    error: string
  }
};

class LoginContainer extends React.Component<{} | State> {
  constructor(props) {
    super(props);
    this.state = {
      loginForm: {
        elements: {
          login: {
            elementType: 'input',
            elementConfig: {
              type: 'email',
              placeholder: 'example@example.com'
            },
            value: '',
            label: 'Email',
            validation: {
              required: true,
              isEmail: true
            },
            valid: false,
            touched: false
          },
          password: {
            elementType: 'input',
            elementConfig: {
              type: 'password',
              placeholder: ''
            },
            value: '',
            label: 'Heslo',
            validation: {
              required: true
            },
            valid: false,
            touched: false
          }
        },
        valid: false,
        error: ''
      }
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  validation = (value, rules) => {
    let isValid = true;
    if (!rules) {
      return isValid;
    }

    if (rules.required) {
      isValid = value.trim() !== '' && isValid;
    }

    if (rules.isEmail) {
      const pattern = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
      isValid = pattern.test(value) && isValid;
    }

    return isValid;
  };

  handleChange = (event, id) => {
    const { loginForm } = this.state;

    const updatedLoginForm = Object.assign({}, loginForm);
    const updatedFormElement = Object.assign({}, updatedLoginForm.elements[id]);

    updatedFormElement.value = event.currentTarget.value;
    updatedFormElement.valid = this.validation(
      updatedFormElement.value,
      updatedFormElement.validation
    );
    updatedFormElement.touched = true;
    updatedLoginForm.elements[id] = updatedFormElement;

    let formIsValid = true;
    Object.keys(updatedLoginForm.elements).forEach(key => {
      formIsValid = updatedLoginForm.elements[key].valid && formIsValid;
    });
    updatedLoginForm.valid = formIsValid;

    this.setState({ loginForm: updatedLoginForm });
  };

  handleSubmit = event => {
    event.preventDefault();
    const { loginForm } = this.state;
    const { login, history, onLoginSubmit } = this.props;

    const formData = {};
    Object.keys(loginForm.elements).forEach(key => {
      formData[key] = loginForm.elements[key].value;
    });

    login({
      variables: {
        login: formData.login,
        password: formData.password
      }
    })
      .then(res => {
        onLoginSubmit(res.data.login);
        history.push('/devices');
      })
      .catch(() => {
        const updatedLoginForm = Object.assign({}, loginForm);
        updatedLoginForm.error = 'Neplatné přihlášení! Zkuste to prosím znovu.';
        this.setState({ loginForm: updatedLoginForm });
      });
  };

  render(): Node {
    const { loginForm } = this.state;
    return (
      <Layout>
        <div className={styles.container}>
          <div className={styles.login}>
            <div className={styles.heading}>
              <h1 className={styles.headingPrimary}>Přihlášení</h1>
              <h3 className={styles.headingSecondary}>
                Zadejte Vaše přihlášení pro přístup do portálu. Po přihlášení si
                můžete telefon vypůjčit nebo ho vrátit.
              </h3>
            </div>

            {loginForm.error ? (
              <div className={styles.error}>
                <h3 className={styles.errorText}>*{loginForm.error}</h3>
              </div>
            ) : null}

            <LoginForm
              form={loginForm}
              handleChange={this.handleChange}
              handleSubmit={this.handleSubmit}
            />
          </div>
        </div>
      </Layout>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  onLoginSubmit: data => dispatch(userAction.login(data))
});

export default compose(
  graphql(loginMutation, { name: 'login' }),
  connect(
    null,
    mapDispatchToProps
  )
)(LoginContainer);
