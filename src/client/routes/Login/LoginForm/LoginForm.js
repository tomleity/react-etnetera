'use strict';

// @flow
import React, { type Node } from 'react';
import Input from '../../../components/UI/Input/Input';
import Button from '../../../components/UI/Button/Button';
import styles from './LoginForm.scss';

type InputType = {
  elementType: string,
  elementConfig: { [param: string]: mixed },
  value: string,
  label: string,
  validation: { [rule: string]: boolean },
  valid: boolean,
  touched: boolean
};

type Props = {
  form: {
    elements: { [name: string]: InputType },
    valid: boolean,
    loading: boolean
  },
  handleSubmit: (SyntheticEvent<HTMLFormElement>) => mixed,
  handleChange: (SyntheticInputEvent<HTMLInputElement>, string) => mixed
};

const LoginForm = (props: Props): Node => {
  const { form, handleSubmit, handleChange } = props;

  const formElements = [];
  Object.keys(form.elements).forEach(key => {
    formElements.push({
      id: key,
      config: form.elements[key]
    });
  });

  return (
    <form onSubmit={handleSubmit}>
      {formElements.map(formElement => (
        <Input
          key={formElement.id}
          id={formElement.id}
          elementType={formElement.config.elementType}
          elementConfig={formElement.config.elementConfig}
          value={formElement.config.value}
          label={formElement.config.label}
          valid={formElement.config.valid}
          touched={formElement.config.touched}
          handleChange={event => handleChange(event, formElement.id)}
        />
      ))}
      <Button type="submit" className={styles.submitButton}>
        Přihlásit
      </Button>
    </form>
  );
};

export default LoginForm;
