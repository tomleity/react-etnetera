'use strict';

// @flow
import React, { type Node } from 'react';
import { graphql } from 'react-apollo';
import { createPhone } from '../../graphql/mutations/phoneMutations';
import Layout from '../../components/Layout/Layout';
import DeviceForm from '../../components/Forms/DeviceForm/DeviceForm';
import styles from './ManageCreate.scss';

type Props = {
  history: { [x: string]: any },
  create: any => any
};

class ManageCreate extends React.Component<Props> {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit = (event, deviceForm) => {
    event.preventDefault();
    const { create, history } = this.props;

    const formData = {};
    Object.keys(deviceForm.elements).forEach(key => {
      formData[key] = deviceForm.elements[key].value;
    });

    create({
      variables: {
        data: formData
      }
    }).then(() => {
      history.push('/manage');
    });
  };

  render(): Node {
    return (
      <Layout>
        <section className={styles.section}>
          <div className={styles.container}>
            <h1 className={styles.heading}>Nové zařízení</h1>
            <DeviceForm submitHandler={this.handleSubmit} />
          </div>
        </section>
      </Layout>
    );
  }
}

export default graphql(createPhone, {
  name: 'create',
  options: {
    refetchQueries: ['fetchPhones']
  }
})(ManageCreate);
