'use strict';

// @flow
import React, { type Node } from 'react';
import Layout from '../../components/Layout/Layout';
import styles from './NoMatch.scss';

class Login extends React.Component {
  render(): Node {
    return (
      <Layout>
        <div className={styles.container}>
          <h1 className={styles.message}>Page not found</h1>
        </div>
      </Layout>
    );
  }
}

export default Login;
