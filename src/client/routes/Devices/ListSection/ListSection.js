'use strict';

// @flow
import React, { type Node } from 'react';
import { connect } from 'react-redux';
import DeviceItem from './DeviceItem/DeviceItem';
import styles from './ListSection.scss';

type Props = {
  user: {
    id: string
  },
  phones: Array<{ [phone: string]: any }>,
  handleDeviceBorrow: (id: string) => mixed,
  handleDeviceReturn: (id: string) => mixed
};

const ListSection = (props: Props): Node => {
  const { user, phones, handleDeviceBorrow, handleDeviceReturn } = props;

  return (
    <section className={styles.section}>
      <div className={styles.container}>
        <div className={styles.list}>
          {phones.map(phone => (
            <DeviceItem
              key={phone.id}
              user={user}
              phone={phone}
              handleDeviceBorrow={handleDeviceBorrow}
              handleDeviceReturn={handleDeviceReturn}
            />
          ))}
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = state => ({
  user: {
    id: state.user.id
  }
});

export default connect(mapStateToProps)(ListSection);
