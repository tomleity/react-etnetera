'use strict';

// @flow
import React, { type Node } from 'react';
import moment from 'moment';
import Button from '../../../../components/UI/Button/Button';
import styles from './DeviceItem.scss';

type Props = {
  user: {
    id: string
  },
  phone: { [phone: string]: any },
  handleDeviceBorrow: (id: string) => mixed,
  handleDeviceReturn: (id: string) => mixed
};

const DeviceItem = (props: Props): Node => {
  const { user, phone, handleDeviceBorrow, handleDeviceReturn } = props;

  let preview = true;
  if (
    !phone.image ||
    (!phone.image.startsWith('www') && !phone.image.startsWith('http'))
  ) {
    preview = false;
  }

  let borrowedDate = null;
  if (phone.borrowed) {
    const date = moment(phone.borrowed.date);
    borrowedDate = date.format('DD.MM.YYYY H:mm');
  }

  let actionButton = null;
  if (!phone.borrowed) {
    actionButton = (
      <Button
        btnStyle="success"
        clicked={() => handleDeviceBorrow(phone.id)}
        className={styles.button}
      >
        Půjčit
      </Button>
    );
  } else if (phone.borrowed.user.id === user.id) {
    actionButton = (
      <Button
        btnStyle="info"
        clicked={() => handleDeviceReturn(phone.id)}
        className={styles.button}
      >
        Vrátit
      </Button>
    );
  } else {
    actionButton = (
      <Button disabled btnStyle="default" className={styles.button}>
        Vypůjčeno
      </Button>
    );
  }

  return (
    <div className={styles.device}>
      <div className={styles.devicePreview}>
        {preview ? (
          <img src={phone.image} alt={phone.model} className={styles.image} />
        ) : (
          <div className={styles.noPreview}>
            <h2 className={styles.noPreviewText}>
              <span className={styles.noPreviewSpan}>No preview</span>
              <span className={styles.noPreviewSpan}>available</span>
            </h2>
          </div>
        )}
      </div>
      <div className={styles.deviceInfo}>
        {phone.borrowed ? (
          <div className={styles.borrowed}>
            <h3 className={styles.borrowedText}>
              <span className={styles.left}>{phone.borrowed.user.name}</span>
              <span className={styles.right}>{borrowedDate}</span>
            </h3>
          </div>
        ) : null}
        <h2 className={styles.heading}>
          <span className={styles.headingPrimary}>{phone.model}</span>
          <span className={styles.headingSecondary}>{phone.vendor}</span>
        </h2>
        <h3 className={styles.os}>
          {phone.os}
          {phone.osVersion ? ` / ${phone.osVersion}` : null}
        </h3>
      </div>
      {actionButton}
    </div>
  );
};

export default DeviceItem;
