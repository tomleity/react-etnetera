'use strict';

// @flow
import React, { type Node } from 'react';
import styles from './StatusSection.scss';

type Props = {
  count: number,
  vendors: number,
  checked: number
};

const StatusSection = (props: Props): Node => {
  const { count, vendors, checked } = props;

  return (
    <section className={styles.section}>
      <div className={styles.headingPrimary}>Telefony</div>
      <div className={styles.headingSecondary}>
        Rezervuj, vyzvedni nebo vyměň
      </div>
      <div className={styles.stats}>
        <h1 className={styles.statsElement}>
          Databáze: <span className={styles.count}>{count}</span>
        </h1>
        <h1 className={styles.statsElement}>
          Značek: <span className={styles.count}>{vendors}</span>
        </h1>
        <h1 className={styles.statsElement}>
          Vypůjčeno: <span className={styles.count}>{checked}</span>
        </h1>
      </div>
    </section>
  );
};

export default StatusSection;
