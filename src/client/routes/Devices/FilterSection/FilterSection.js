'use strict';

// @flow
import React, { type Node } from 'react';
import Input from '../../../components/UI/Input/Input';
import styles from './FilterSection.scss';

type Props = {
  values: {
    vendor: Array<string>,
    os: Array<string>,
    availability: string,
    search: string
  },
  valuesOptions: {
    [input: string]: Array<{
      value: string,
      displayValue: string
    }>
  },
  handleFilterChange: (SyntheticInputEvent<HTMLInputElement>) => mixed
};

const FilterSection = (props: Props): Node => {
  const { values, valuesOptions, handleFilterChange } = props;

  return (
    <section className={styles.section}>
      <div className={styles.container}>
        <form className={styles.form}>
          <Input
            id="vendor"
            inline
            elementType="select"
            elementConfig={{
              options: valuesOptions.vendor
            }}
            value={values.vendor}
            className={{
              field: styles.field
            }}
            handleChange={handleFilterChange}
          />
          <Input
            id="os"
            inline="true"
            elementType="select"
            elementConfig={{
              options: valuesOptions.os
            }}
            value={values.os}
            className={{
              field: styles.field
            }}
            handleChange={handleFilterChange}
          />
          <Input
            id="availability"
            inline="true"
            elementType="select"
            elementConfig={{
              options: valuesOptions.availability
            }}
            value={values.availability}
            className={{
              field: styles.field
            }}
            handleChange={handleFilterChange}
          />
          <Input
            id="search"
            inline="true"
            elementType="input"
            elementConfig={{
              type: 'text',
              placeholder: 'Search...'
            }}
            value={values.search}
            className={{
              field: styles.searchField
            }}
            handleChange={handleFilterChange}
          />
        </form>
      </div>
    </section>
  );
};

export default FilterSection;
