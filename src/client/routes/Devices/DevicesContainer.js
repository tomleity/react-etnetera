'use strict';

// @flow
import React, { type Node } from 'react';
// $FlowFixMe
import { graphql, compose } from 'react-apollo';
import debounce from 'lodash.debounce';

import { fetchPhones } from '../../graphql/queries/phoneQueries';
import {
  borrowPhone,
  returnPhone
} from '../../graphql/mutations/phoneMutations';
import Layout from '../../components/Layout/Layout';
import Devices from './Devices';

type State = {
  filter: {
    vendor: string,
    os: string,
    availability: string,
    search: string
  }
};

type Props = {
  data: { [x: string]: any },
  borrowDevice: any => any,
  returnDevice: any => any
};

class DevicesContainer extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      filter: {
        vendor: '',
        os: '',
        availability: '',
        search: ''
      }
    };

    this.handleRefetch = debounce(this.handleRefetch.bind(this), 1000);
    this.handleFilterChange = this.handleFilterChange.bind(this);
    this.handleDeviceBorrow = debounce(
      this.handleDeviceBorrow.bind(this),
      1000
    );
    this.handleDeviceReturn = debounce(
      this.handleDeviceReturn.bind(this),
      1000
    );
  }

  checkFilter = filterState => {
    const filter = Object.assign({}, filterState);

    if (filter.availability !== '') {
      filter.available = filter.availability === 'true';
      delete filter.availability;
    } else {
      delete filter.availability;
    }

    const checked = {};
    Object.keys(filter).forEach(key => {
      if (filter[key] !== '') {
        checked[key] = filter[key];
      }
    });

    return checked;
  };

  handleRefetch = updatedFilter => {
    const { data } = this.props;
    const filter = this.checkFilter(updatedFilter);

    data.refetch({ filter });
  };

  handleFilterChange = event => {
    const { id, value } = event.currentTarget;
    const { filter } = this.state;

    const updatedFilter = Object.assign({}, filter);
    updatedFilter[id] = value;

    this.setState({ filter: updatedFilter });
    this.handleRefetch(updatedFilter);
  };

  handleDeviceBorrow = id => {
    const { filter } = this.state;
    const { borrowDevice } = this.props;

    const checkedFilter = this.checkFilter(filter);

    borrowDevice({
      variables: { id },
      refetchQueries: [
        {
          query: fetchPhones,
          variables: {
            filter: checkedFilter
          }
        }
      ]
    });
  };

  handleDeviceReturn = id => {
    const { filter } = this.state;
    const { returnDevice } = this.props;

    const checkedFilter = this.checkFilter(filter);

    returnDevice({
      variables: { id },
      refetchQueries: [
        {
          query: fetchPhones,
          variables: {
            filter: checkedFilter
          }
        }
      ]
    });
  };

  render(): Node {
    const { filter } = this.state;
    const { data } = this.props;

    return (
      <Layout header={{ solid: false }}>
        <Devices
          data={data}
          filter={filter}
          handleFilterChange={this.handleFilterChange}
          handleDeviceBorrow={this.handleDeviceBorrow}
          handleDeviceReturn={this.handleDeviceReturn}
        />
      </Layout>
    );
  }
}

export default compose(
  graphql(fetchPhones),
  graphql(borrowPhone, { name: 'borrowDevice' }),
  graphql(returnPhone, { name: 'returnDevice' })
)(DevicesContainer);
