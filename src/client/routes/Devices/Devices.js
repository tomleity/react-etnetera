'use strict';

// @flow
import React, { type Node } from 'react';
import AuxWrapper from '../../hoc/AuxWrapper/AuxWrapper';
import StatusSection from './StatusSection/StatusSection';
import FilterSection from './FilterSection/FilterSection';
import ListSection from './ListSection/ListSection';

type Props = {
  data: { [x: string]: any },
  filter: {
    vendor: string,
    os: string,
    availability: string,
    search: string
  },
  handleFilterChange: (SyntheticInputEvent<HTMLInputElement>) => mixed,
  handleDeviceBorrow: (id: string) => mixed,
  handleDeviceReturn: (id: string) => mixed
};

const getFilterOptions = fetchedOptions => {
  const options = {
    vendor: [{ value: '', displayValue: 'Vybrat značku' }],
    os: [{ value: '', displayValue: 'Vybrat systém' }],
    availability: [
      { value: '', displayValue: 'Dostupnost' },
      { value: 'true', displayValue: 'Dostupné' },
      { value: 'false', displayValue: 'Vypůjčené' }
    ]
  };

  if (fetchedOptions) {
    Object.keys(fetchedOptions).forEach(key => {
      if (options[key]) {
        options[key].push(
          ...fetchedOptions[key].map(option => ({
            value: option,
            displayValue: option
          }))
        );
      }
    });
  }

  return options;
};

const Phones = (props: Props): Node => {
  const {
    data,
    filter,
    handleFilterChange,
    handleDeviceBorrow,
    handleDeviceReturn
  } = props;

  const phones = data.phones ? data.phones.data : [];
  const status = {
    count: data.phones ? data.phones.count : 0,
    vendors: data.phones ? data.phones.options.vendor.length : 0,
    checked: data.phones ? data.phones.borrowed : 0
  };
  const filterOptions = getFilterOptions(
    data.phones ? data.phones.options : null
  );

  return (
    <AuxWrapper>
      <StatusSection
        count={status.count}
        vendors={status.vendors}
        checked={status.checked}
      />
      <FilterSection
        values={filter}
        valuesOptions={filterOptions}
        handleFilterChange={handleFilterChange}
      />
      <ListSection
        phones={phones}
        handleDeviceBorrow={handleDeviceBorrow}
        handleDeviceReturn={handleDeviceReturn}
      />
    </AuxWrapper>
  );
};

export default Phones;
