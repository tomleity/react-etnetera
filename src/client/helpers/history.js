'use strict';

/* eslint global-require: 0 */

const createHistory =
  process.env.NODE_ENV === 'development'
    ? require('history/createHashHistory').default
    : require('history/createBrowserHistory').default;

const history = createHistory();

export default history;
