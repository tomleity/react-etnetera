'use strict';

const req = require.context('../assets/svg', true, /^\.\/.*\.svg$/);

export default req.keys().reduce((g, key) => {
  const filename = key.match(new RegExp(/[^/]+(?=\.svg$)/))[0];
  return Object.assign({}, g, { [filename]: req(key) });
}, {});
