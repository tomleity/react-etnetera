'use strict';

import Enzyme, { configure, swallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

export { swallow, mount, render };
export default Enzyme;
