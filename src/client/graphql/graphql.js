'use strict';

import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { setContext } from 'apollo-link-context';
import { createHttpLink } from 'apollo-link-http';

const httpLink = createHttpLink({
  uri: '/graphql'
});

const authLink = setContext((req, { headers }) => {
  const userStorage = localStorage.getItem('user');
  const user = JSON.parse(userStorage);

  return {
    headers: {
      ...headers,
      'Auth-Token': user ? user.token : null
    }
  };
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache()
  // cache: new InMemoryCache({
  //   dataIdFromObject: object => object.id || null
  // })
});

export default client;
