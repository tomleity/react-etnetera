'use strict';

import gql from 'graphql-tag';

export const fetchPhone = gql`
  query fetchPhone($id: ID!) {
    phone(id: $id) {
      id
      code
      os
      vendor
      model
      osVersion
      image
      borrowed {
        date
        user {
          id
          type
          login
          name
        }
      }
    }
  }
`;

export const fetchPhones = gql`
  query fetchPhones($filter: PhonesFilter) {
    phones(filter: $filter) {
      count
      borrowed
      filterCount
      filter {
        vendor
        os
        available
        search
      }
      options {
        vendor
        os
      }
      data {
        id
        code
        os
        vendor
        model
        osVersion
        image
        borrowed {
          date
          user {
            id
            type
            login
            name
          }
        }
      }
    }
  }
`;
