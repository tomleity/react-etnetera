'use strict';

import gql from 'graphql-tag';

export const fetchUser = gql`
  query fetchUser($id: ID!) {
    user(id: $id) {
      id
      name
      type
      login
    }
  }
`;
