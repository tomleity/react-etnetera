'use strict';

import gql from 'graphql-tag';

export const createPhone = gql`
  mutation createPhone($data: PhoneCreateData!) {
    createPhone(data: $data) {
      id
      code
      os
      vendor
      model
      osVersion
      image
    }
  }
`;

export const updatePhone = gql`
  mutation updatePhone($id: ID!, $data: PhoneUpdateData!) {
    updatePhone(id: $id, data: $data) {
      id
      code
      os
      vendor
      model
      osVersion
      image
      borrowed {
        date
        user {
          id
          type
          login
          name
        }
      }
    }
  }
`;

export const deletePhone = gql`
  mutation deletePhone($id: ID!) {
    deletePhone(id: $id) {
      id
    }
  }
`;

export const borrowPhone = gql`
  mutation borrowPhone($id: ID!) {
    borrowPhone(id: $id) {
      id
    }
  }
`;

export const returnPhone = gql`
  mutation returnPhone($id: ID!) {
    returnPhone(id: $id) {
      id
    }
  }
`;
