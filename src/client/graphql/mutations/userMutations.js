'use strict';

import gql from 'graphql-tag';

export const loginMutation = gql`
  mutation login($login: String!, $password: String!) {
    login(login: $login, password: $password) {
      id
      name
      type
      login
      token
    }
  }
`;
