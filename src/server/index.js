'use strict';

/* eslint global-require: 0 */

import http from 'http';
import path from 'path';
import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import chalk from 'chalk';

import GraphQLExpress from 'express-graphql';
import schema from './schema/schema';

// Unhandled Promise rejections
process.on('unhandledRejection', reason => {
  console.error(`Unhandled 'Promise' rejection`);
  console.error(reason);
  process.exit(1);
});

// App instance
const app = express();

// Trusted Proxy
app.set('trust proxy', 'loopback');

// Middleware
app.use(compression());
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(
  '/graphql',
  GraphQLExpress(req => ({
    schema,
    context: {
      token: req.get('Auth-Token')
    },
    graphiql: process.env.NODE_ENV === 'development'
  }))
);

app.use(express.static(path.resolve(__dirname, 'public')));
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'public/index.html'));
});

// Production
if (!module.parent && !module.hot) {
  const port = process.env.APP_PORT;
  const host = process.env.APP_HOST;

  http.createServer(app).listen(port, host, err => {
    if (err) {
      throw new Error(err);
    }
    console.info(
      `Running in "${chalk.green(process.env.NODE_ENV.toUpperCase())}" mode`
    );
    console.info(chalk.blue(`Server is listening on: ${host}:${port}`));
  });
}

// Development
if (module.hot) {
  app.hot = module.hot;
}

export default app;
