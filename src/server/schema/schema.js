'use strict';

import { GraphQLSchema } from 'graphql';

import RootQueryType from './types';
import mutations from './mutations';

const schema = new GraphQLSchema({
  query: RootQueryType,
  mutation: mutations
});

export default schema;
