'use strict';

import axios from '../../services/axios';

const login = data =>
  axios
    .login(data)
    .then(res => res.data)
    .catch(err => {
      throw new Error(err);
    });

const fetchUser = (id, token) =>
  axios
    .fetchUser(id, token)
    .then(res => res.data)
    .catch(err => {
      throw new Error(err);
    });

export default { login, fetchUser };
