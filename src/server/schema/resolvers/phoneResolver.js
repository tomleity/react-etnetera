'use strict';

import axios from '../../services/axios';

const fetchPhone = (id, token) =>
  axios
    .fetchPhone(id, token)
    .then(res => res.data)
    .catch(err => {
      throw new Error(err);
    });

const fetchPhones = async (filter, token) => {
  const data = await axios
    .fetchPhones(token)
    .then(res => res.data)
    .catch(err => {
      throw new Error(err);
    });

  const options = {
    vendor: [],
    os: []
  };

  let borrowedCount = 0;
  const filtered = data.filter(phone => {
    options.vendor.push(phone.vendor);
    options.os.push(phone.os);

    if (phone.borrowed) borrowedCount += 1;

    if (filter) {
      if (filter.vendor && filter.vendor.length !== 0) {
        if (!filter.vendor.includes(phone.vendor)) return false;
      }
      if (filter.os && filter.os.length !== 0) {
        if (!filter.os.includes(phone.os)) return false;
      }
      if (typeof filter.available !== 'undefined') {
        if (!filter.available && !phone.borrowed) return false;
        if (filter.available && phone.borrowed) return false;
      }
      if (filter.search) {
        const compare = phone.model.toLowerCase();
        if (!compare.includes(filter.search.toLowerCase())) return false;
      }
    }
    return true;
  });

  return {
    count: data.length,
    borrowed: borrowedCount,
    options: {
      vendor: [...new Set(options.vendor)],
      os: [...new Set(options.os)]
    },
    filter,
    filterCount: filtered.length,
    data: filtered.reverse()
  };
};

const createPhone = (data, token) =>
  axios
    .createPhone(data, token)
    .then(res => res.data)
    .catch(err => {
      throw new Error(err);
    });

const updatePhone = (id, data, token) =>
  axios
    .updatePhone(id, data, token)
    .then(res => res.data)
    .catch(err => {
      throw new Error(err);
    });

const deletePhone = (id, token) =>
  axios
    .deletePhone(id, token)
    .then(res => res.data)
    .catch(err => {
      throw new Error(err);
    });

const borrowPhone = (id, token) =>
  axios
    .borrowPhone(id, token)
    .then(res => res.data)
    .catch(err => {
      throw new Error(err);
    });

const returnPhone = (id, token) =>
  axios
    .returnPhone(id, token)
    .then(res => res.data)
    .catch(err => {
      throw new Error(err);
    });

export default {
  fetchPhone,
  fetchPhones,
  createPhone,
  updatePhone,
  deletePhone,
  borrowPhone,
  returnPhone
};
