'use strict';

import {
  GraphQLInputObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLID
} from 'graphql';
import { PhoneType } from '../types/phoneType';
import phoneResolver from '../resolvers/phoneResolver';

const createPhone = {
  type: PhoneType,
  args: {
    data: {
      type: new GraphQLNonNull(
        new GraphQLInputObjectType({
          name: 'PhoneCreateData',
          fields: () => ({
            code: { type: new GraphQLNonNull(GraphQLString) },
            os: { type: new GraphQLNonNull(GraphQLString) },
            vendor: { type: new GraphQLNonNull(GraphQLString) },
            model: { type: new GraphQLNonNull(GraphQLString) },
            osVersion: { type: GraphQLString },
            image: { type: GraphQLString }
          })
        })
      )
    }
  },
  resolve(type, { data }, context) {
    return phoneResolver.createPhone(data, context.token);
  }
};

const updatePhone = {
  type: PhoneType,
  args: {
    id: { type: new GraphQLNonNull(GraphQLID) },
    data: {
      type: new GraphQLNonNull(
        new GraphQLInputObjectType({
          name: 'PhoneUpdateData',
          fields: () => ({
            code: { type: new GraphQLNonNull(GraphQLString) },
            os: { type: new GraphQLNonNull(GraphQLString) },
            vendor: { type: new GraphQLNonNull(GraphQLString) },
            model: { type: new GraphQLNonNull(GraphQLString) },
            osVersion: { type: GraphQLString },
            image: { type: GraphQLString }
          })
        })
      )
    }
  },
  resolve(type, { id, data }, context) {
    return phoneResolver.updatePhone(id, data, context.token);
  }
};

const deletePhone = {
  type: PhoneType,
  args: {
    id: { type: new GraphQLNonNull(GraphQLID) }
  },
  resolve(type, { id }, context) {
    return phoneResolver.deletePhone(id, context.token);
  }
};

const borrowPhone = {
  type: PhoneType,
  args: {
    id: { type: new GraphQLNonNull(GraphQLID) }
  },
  resolve(type, { id }, context) {
    return phoneResolver.borrowPhone(id, context.token);
  }
};

const returnPhone = {
  type: PhoneType,
  args: {
    id: { type: new GraphQLNonNull(GraphQLID) }
  },
  resolve(type, { id }, context) {
    return phoneResolver.returnPhone(id, context.token);
  }
};

export default {
  createPhone,
  updatePhone,
  deletePhone,
  borrowPhone,
  returnPhone
};
