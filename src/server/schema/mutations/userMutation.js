'use strict';

import { GraphQLString, GraphQLNonNull } from 'graphql';
import { UserLoginType } from '../types/userType';
import userResolver from '../resolvers/userResolver';

const login = {
  type: UserLoginType,
  args: {
    login: { type: new GraphQLNonNull(GraphQLString) },
    password: { type: new GraphQLNonNull(GraphQLString) }
  },
  resolve(type, data) {
    return userResolver.login(data);
  }
};

export default { login };
