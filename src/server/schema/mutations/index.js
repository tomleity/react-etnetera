'use strict';

import { GraphQLObjectType } from 'graphql';

import userMutation from './userMutation';
import phoneMutation from './phoneMutation';

const mutations = new GraphQLObjectType({
  name: 'Mutation',
  fields: () => ({
    ...userMutation,
    ...phoneMutation
  })
});

export default mutations;
