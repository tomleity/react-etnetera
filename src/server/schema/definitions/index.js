'use strict';

import DateTime from './DateTimeScalar';

export const GraphQLDateTime = DateTime;
