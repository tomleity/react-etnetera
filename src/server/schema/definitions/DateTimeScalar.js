'use strict';

const { GraphQLScalarType } = require('graphql');
const { isISO8601 } = require('validator');

const error = () => {
  throw new Error('DateTime cannot represent an invalid ISO-8601 Date string');
};

module.exports = new GraphQLScalarType({
  name: 'DateTime',
  description: 'An ISO-8601 encoded UTC date string.',
  serialize: value => (isISO8601(value) ? value : error),
  parseValue: value => (isISO8601(value) ? value : error),
  parseLiteral: ast => (isISO8601(ast.value) ? ast.value : error)
});
