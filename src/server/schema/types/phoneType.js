'use strict';

import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLFloat,
  GraphQLList,
  GraphQLBoolean,
  GraphQLID
} from 'graphql';
import { UserType } from './userType';

const BorrowType = new GraphQLObjectType({
  name: 'BorrowType',
  fields: () => ({
    user: { type: UserType },
    date: { type: GraphQLFloat }
  })
});

const PhoneType = new GraphQLObjectType({
  name: 'PhoneType',
  fields: () => ({
    id: { type: GraphQLID },
    code: { type: GraphQLString },
    os: { type: GraphQLString },
    vendor: { type: GraphQLString },
    model: { type: GraphQLString },
    osVersion: { type: GraphQLString },
    image: { type: GraphQLString },
    borrowed: { type: BorrowType }
  })
});

const PhonesType = new GraphQLObjectType({
  name: 'PhonesType',
  fields: () => ({
    count: { type: GraphQLString },
    borrowed: { type: GraphQLString },
    options: {
      type: new GraphQLObjectType({
        name: 'PhonesOptionsType',
        fields: () => ({
          vendor: { type: new GraphQLList(GraphQLString) },
          os: { type: new GraphQLList(GraphQLString) }
        })
      })
    },
    filterCount: { type: GraphQLString },
    filter: {
      type: new GraphQLObjectType({
        name: 'PhonesFilterType',
        fields: () => ({
          vendor: { type: new GraphQLList(GraphQLString) },
          os: { type: new GraphQLList(GraphQLString) },
          available: { type: GraphQLBoolean },
          search: { type: GraphQLString }
        })
      })
    },
    data: { type: new GraphQLList(PhoneType) }
  })
});

export { PhoneType, PhonesType };
