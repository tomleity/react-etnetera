'use strict';

import {
  GraphQLInputObjectType,
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLList,
  GraphQLBoolean,
  GraphQLID
} from 'graphql';

import { UserType } from './userType';
import { PhoneType, PhonesType } from './phoneType';
import userResolver from '../resolvers/userResolver';
import phoneResolver from '../resolvers/phoneResolver';

const user = {
  type: UserType,
  args: {
    id: { type: new GraphQLNonNull(GraphQLID) }
  },
  resolve(type, { id }, context) {
    return userResolver.fetchUser(id, context.token);
  }
};

const phone = {
  type: PhoneType,
  args: {
    id: { type: new GraphQLNonNull(GraphQLID) }
  },
  resolve(type, { id }, context) {
    return phoneResolver.fetchPhone(id, context.token);
  }
};

const phones = {
  type: PhonesType,
  args: {
    filter: {
      type: new GraphQLInputObjectType({
        name: 'PhonesFilter',
        fields: () => ({
          vendor: { type: new GraphQLList(GraphQLString) },
          os: { type: new GraphQLList(GraphQLString) },
          available: { type: GraphQLBoolean },
          search: { type: GraphQLString }
        })
      })
    }
  },
  resolve(type, { filter }, context) {
    return phoneResolver.fetchPhones(filter, context.token);
  }
};

export default new GraphQLObjectType({
  name: 'RootQueryType',
  fields: () => ({ user, phones, phone })
});
