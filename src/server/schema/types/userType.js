'use strict';

import { GraphQLObjectType, GraphQLString, GraphQLID } from 'graphql';

const UserType = new GraphQLObjectType({
  name: 'UserType',
  fields: () => ({
    id: { type: GraphQLID },
    type: { type: GraphQLString },
    login: { type: GraphQLString },
    name: { type: GraphQLString }
  })
});

const UserLoginType = new GraphQLObjectType({
  name: 'UserLoginType',
  fields: () => ({
    id: { type: GraphQLID },
    type: { type: GraphQLString },
    login: { type: GraphQLString },
    name: { type: GraphQLString },
    token: { type: GraphQLString }
  })
});

export { UserType, UserLoginType };
