'use strict';

import axios from 'axios';

const api = axios.create({
  baseURL: process.env.APP_API_ENDPOINT,
  responseType: 'json',
  timeout: 1000,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json'
  }
});

export default {
  login: data => api.post(`/login`, data),
  fetchUser: (id, token) =>
    api.get(`/users/${id}`, {
      headers: { 'Auth-Token': token }
    }),
  fetchPhones: token =>
    api.get(`/phones`, {
      headers: { 'Auth-Token': token }
    }),
  fetchPhone: (id, token) =>
    api.get(`/phones/${id}`, {
      headers: { 'Auth-Token': token }
    }),
  createPhone: (data, token) =>
    api.post(`/phones`, data, {
      headers: { 'Auth-Token': token }
    }),
  updatePhone: (id, data, token) =>
    api.put(`/phones/${id}`, data, {
      headers: { 'Auth-Token': token }
    }),
  deletePhone: (id, token) =>
    api.delete(`/phones/${id}`, {
      headers: { 'Auth-Token': token }
    }),
  borrowPhone: (id, token) =>
    api.post(`/phones/${id}/borrow`, null, {
      headers: { 'Auth-Token': token }
    }),
  returnPhone: (id, token) =>
    api.post(`/phones/${id}/return`, null, {
      headers: { 'Auth-Token': token }
    })
};
