/* eslint global-require: 0 */

const pkg = require('./package.json');

/**
 * PostCSS configuration
 * https://github.com/postcss/postcss/tree/master/docs
 */
module.exports = () => ({
  plugins: [
    require('cssnano')({
      preset: [
        'default',
        {
          discardComments: {
            removeAll: true
          }
        }
      ]
    }),
    require('postcss-import')(),
    require('postcss-flexbugs-fixes')(),
    require('autoprefixer')({
      browsers: pkg.browserslist,
      flexbox: 'no-2009'
    })
  ]
});
