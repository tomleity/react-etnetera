'use strict';

/**
 * Prettier configuration
 * https://prettier.io/docs/en/configuration.html
 */
module.exports = {
  printWidth: 80,
  singleQuote: true,
  trailingComma: 'none'
};
